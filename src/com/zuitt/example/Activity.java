package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Activity {
    public static void main(String[] args){
        HashMap<String, Integer> games = new HashMap<>();
        games.put("Mario Odyssey", 50);
        games.put("Super Smash Bros. Ultimate", 20);
        games.put("Luigi's Mansion 3", 15);
        games.put("Pokemon Sword", 30);
        games.put("Pokemon Shield", 100);

        games.forEach((key, value) -> {
            System.out.println(key + " has " + value + " stocks left.");
        });

        ArrayList<String> topGames = new ArrayList<>();
        games.forEach((key, value) -> {
            if(value <= 30) {
                topGames.add(key);
                System.out.println(key + " has been added to top games list!");
            }
        });
        System.out.println(topGames);

        boolean addItem = true;
        while(addItem == true){
            System.out.println("Would you like to add an item? Yes or No.");
            Scanner input = new Scanner(System.in);
            String userInput = input.nextLine();
            if(userInput.equals("Yes")){
                System.out.println("Add the item name");
                String itemName = input.nextLine();
                System.out.println("Add the item stock");
                int itemStock = input.nextInt();
                games.put(itemName, itemStock);
                System.out.println(itemName + " has been added with " + itemStock + " stocks");
                addItem = false;
            } else if (userInput.equals("No")){
                System.out.println("Thank you");
                addItem = false;
            } else {
                System.out.println("Invalid input. Try again.");
            }
        }
    }
}
